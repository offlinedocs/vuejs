# Build
FROM node:latest AS builder

RUN git clone https://github.com/vuejs/vuejs.org.git /vuejs.org

WORKDIR /vuejs.org

RUN npm install
RUN npm run build

# Serve
FROM nginx:alpine

COPY --from=builder /vuejs.org/public /usr/share/nginx/html
